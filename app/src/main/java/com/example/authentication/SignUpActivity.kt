package com.example.authentication

import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init(){
        auth = Firebase.auth
        signUpButton.setOnClickListener{
            signUp()
        }
    }

    private fun signUp(){
        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()
        val repeatPassword = repeatPasswrodEditText.text.toString()

        if(email.isNotEmpty() && password.isNotEmpty()){
            if(password==repeatPassword){
                progressBar.visibility = View.VISIBLE
                signUpButton.isClickable = false
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        progressBar.visibility = View.GONE
                        signUpButton.isClickable = true
                        if (task.isSuccessful) {
                            d("signUp", "createUserWithEmail:success")
                            val user = auth.currentUser
                            Toast.makeText(this,"SignUp is Success",Toast.LENGTH_SHORT).show()
                        } else {
                            // If sign in fails, display a message to the user.
                            d("signUp", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                        }

                    }


            }else{
            Toast.makeText(this,"Please fill correct Password!",Toast.LENGTH_SHORT).show()
        }
            }else{
            Toast.makeText(this,"Please fill all fields!",Toast.LENGTH_SHORT).show()
        }
    }
}